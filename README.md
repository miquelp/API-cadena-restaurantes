# Qué es este proyecto?

API para una cadena de restaurantes. Hay varios restaurantes distribuidos por una zona y los usuarios pueden realizar pedidos que se asignarán al repartidor de la tienda más cercana al cliente en cuestión que haya hecho el pedido.

En mi caso la API será usada en dos app Android, una para los usuarios que harán pedidos y otra para los repartidores de cada tienda. 


# Instalación y prueba

La API está implementada en PHP y sus funciones o servicios se encuentran dentro del directorio *services/*. Los parámetros necesarios para hacer las peticiones HTTP a cada función se tienen que mandar por POST.

Para probar la API, se deben seguir estos pasos:

1) Importar la base de datos que se encuentra en *database/bbdd-cadena-restaurantes.sql* a nuestro servidor mysql (mediante phpMyAdmin, por ejemplo)

2) Crear un directorio (p.e. *API-cadena-restaurantes/*) dentro de */var/www/* de nuestro localhost o servidor y mover el proyecto (todas los directorios y archivos) dentro de dicho directorio. 

3) Actualizar los datos de configuración de la BD en el fichero *database/config.php* introduciendo el host, nuestro usuario y passwor de mysql, y el nombre de la base de datos que hemos importado.

4) Para probar las funciones de la API, acceder mediante un navegador al fichero *testAPI.html* (ej: http://localhost/API-cadena-restaurantes/testAPI.html)  que se encuentra en la raíz del respositorio. Es importante que este fichero esté en la raíz, puesto que las llamadas a la API están definidas de forma relativa y NO absoluta.


