<?php

include_once '../database/db_functions.php';
//Create Object for DB_Functions clas
$db = new DB_Functions();
//Get JSON posted by Android Applicationi
$idUser = $_POST["idUser"];
$nombre = $_POST["nombreUser"];
$lat = $_POST["latUser"];
$long = $_POST["longiUser"];
$pass = $_POST["passwordUser"];
$alias = $_POST["aliasUser"];

$insert = $db->insertUser($idUser, $nombre, $lat, $long, $pass, $alias);

$resultado = array();

if($insert){

 $resultado['result'] = 'ok';

 $user = $db->getUser($idUser);

 $row = mysqli_fetch_Array($user);

 $resultado["idUsuario"] = $row["idUsuario"];
 $resultado["nombre"] = $row["nombre"];
 $resultado["latUsuario"] = $row["latUsuario"];
 $resultado["lonUsuario"] = $row["lonUsuario"];
 $resultado["password"] = $row["pass"];
 $resultado["alias"] = $row["alias"];


}else{

 $resultado['result'] = 'error';

}

echo json_encode($resultado);


?>

