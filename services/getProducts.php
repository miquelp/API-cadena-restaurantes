<?php
include_once '../database/db_functions.php';
//Create Object for DB_Functions clas
$db = new DB_Functions(); 
//Get JSON posted by Android Application

$products = $db->getProducts();

$resultados = array();
$aux = array();


while ($row = mysqli_fetch_array($products)) {

	$aux["idProducto"] = $row["idProducto"];
	$aux["nombre"] = $row["nombre"];
	$aux["descripcion"] = $row["descripcion"];
	$aux['precio'] = $row['precio'];
	$aux['tipo'] = $row['tipo'];
	array_push($resultados, $aux);
}


echo json_encode($resultados);


?>
