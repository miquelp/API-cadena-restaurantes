<?php
include_once '../database/db_functions.php';
//Create Object for DB_Functions clas
$db = new DB_Functions(); 
//Get JSON posted by Android Application

$idUsuario = $_POST["idUsuario"];

$products = $db->getInforPaquete($idUsuario);

$resultados = array();
$aux = array();

while($row = mysqli_fetch_array($products)){

    $aux["idPaquete"] = $row["idPaquete"];
    $aux["latPaquete"] = $row["latRepartidor"];
    $aux["lonPaquete"] = $row["lonRepartidor"];
    $aux["repartidor"] = $row["nombre"];
    $aux["tiempoAprox"] = $row["tiempoAprox"];
    array_push($resultados, $aux);

}

echo json_encode($resultados);


?>