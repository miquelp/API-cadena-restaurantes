<?php

include_once '../database/db_functions.php';
//Create Object for DB_Functions clas
$db = new DB_Functions();
//Get JSON posted by Android Applicationi
$idUser = $_POST["idUser"];
$pedidos = $_POST["pedidos"];

$metrosPorMinuto = 750;
$tiempoCocina = 35;

$locationUser = $db->getUserLocation($idUser);
$row = mysqli_fetch_array($locationUser);

$latUsuario = $row["latUsuario"];
$lonUsuario = $row["lonUsuario"];

$storeLocation =  $db->getStoresLocation();

$minDistance = 99999999999999;
$closestStore = 0;

while($store = mysqli_fetch_array($storeLocation)){

    $latTienda = $store["latTienda"];
    $lonTienda = $store["lonTienda"];
    $idTienda = $store["idTienda"];
    
    $havDistance = haversine($latUsuario, $lonUsuario, $latTienda, $lonTienda);
    
    if($havDistance < $minDistance){
    
        $closestStore = $idTienda;
        $minDistance = $havDistance;
    
    }
   
}


$randomRepartidor =  $db->getRepartidorFromStore($closestStore);

$repartidor = mysqli_fetch_array($randomRepartidor);



$time = $tiempoCocina + calculateTime($minDistance, $metrosPorMinuto);

echo "Distancia: $minDistance TIEMPO: ".$time."<br>";

$insertPaquete =  $db->insertPaquete("preparando", $repartidor["idRepartidor"] , $idUser, $time);

$pedidosArray = json_decode($pedidos);

$getIdPaquete = $db->getLastPaqueteFromUser($idUser);
$getIdPaqueteAux = mysqli_fetch_array($getIdPaquete);

$idPaquete = $getIdPaqueteAux["idPaquete"];

foreach($pedidosArray as $item) { 
    /*echo "idProducto: ".$item->idProducto."<br>";
    echo "cantidad: ".$item->cantidad."<br>";
    echo "dueno".$item->dueno."<br>";*/
    
    $idProd = $item->idProducto;
    $cantidadProd = $item->cantidad;
    
    $insertPedido =  $db->insertPedido($idPaquete, $idProd, $cantidadProd, $time);
}

$resultado = array();

$resultado['result'] = 'ok';



echo json_encode($resultado);
//$insertPaquete = $db->insertPaquete();

function haversine($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo){

  $earthRadius = 6371000;
  // convert from degrees to radians
  $latFrom = deg2rad($latitudeFrom);
  $lonFrom = deg2rad($longitudeFrom);
  $latTo = deg2rad($latitudeTo);
  $lonTo = deg2rad($longitudeTo);

  $latDelta = $latTo - $latFrom;
  $lonDelta = $lonTo - $lonFrom;

  $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
    cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
  return $angle * $earthRadius;
}

function calculateTime($distance, $speed){
    
    return $distance / $speed;

}




?>