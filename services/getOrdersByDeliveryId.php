<?php
include_once '../database/db_functions.php';
//Create Object for DB_Functions clas
$db = new DB_Functions(); 
//Get JSON posted by Android Application
$IDrepartidor = $_POST["IDrepartidor"];

$deliveries = $db->getOrdersByDeliveryId($IDrepartidor);

$resultados = array();
$aux = array();

while ($row = mysqli_fetch_array($deliveries)) {

	$aux["idPaquete"] = $row["idPaquete"];
	$aux["estado"] = $row["estado"];
	$aux["cliente"] = $row["nombre"];
	$aux['dniCliente'] = $row['idUsuario'];
	$aux['latUsuario'] = $row['latUsuario'];
	$aux['lonUsuario'] = $row['lonUsuario'];
	$aux['latRepartidor'] = $row['latRepartidor'];
	$aux['lonRepartidor'] = $row['lonRepartidor'];
	
	
	array_push($resultados, $aux);
}

echo json_encode($resultados);


?>
