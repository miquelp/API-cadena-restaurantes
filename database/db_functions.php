<?php

class DB_Functions {

    private $db;
    private $con;

    //put your code here
    // constructor
    function __construct() {
        include_once 'db_connect.php';
        // connecting to database
        $this->db = new DB_Connect();
        $this->con = $this->db->connect();
    }

    // destructor
    function __destruct() {

    }
    
    /**
     * Returns all orders assigned to a delivery
     * 
     */
       
function getOrdersByDeliveryId($idRepartidor) {

       $query = "SELECT paquete.idPaquete, paquete.estado, usuarios.nombre, usuarios.idUsuario, usuarios.latUsuario, usuarios.lonUsuario, repartidores.latRepartidor, repartidores.lonRepartidor FROM repartidores
        INNER JOIN paquete 
        ON paquete.rAsignado = repartidores.idRepartidor
    INNER JOIN usuarios
       ON usuarios.idUsuario = paquete.dueno
    WHERE repartidores.idRepartidor = $idRepartidor AND paquete.estado <>'cancelado' AND paquete.estado <> 'pagado'";

        #$query = "SELECT * FROM paquete WHERE paquete.rAsignado = $idRepartidor";
        $result = mysqli_query($this->con, $query);
        return $result;
       
    }


    public function updatePedido($idPedido){
	
	$query="UPDATE paquete SET estado='pagado' WHERE idPaquete = ".$idPedido;
    	$result = mysqli_query($this->con, $query);
	return $result;
    
    }
    
    public function insertUser($idUsuario, $name, $lat, $long, $password, $alias){

	$pass = md5($password);

        $query = "INSERT INTO usuarios(idUsuario, nombre, latUsuario, lonUsuario, pass, alias) VALUES($idUsuario, '$name', $lat, $long, '$pass', '$alias')";

        $result = mysqli_query($this->con, $query);

        return $result;

    }
    
    
    
    public function insertPaquete($estado, $rAsignado, $dueno, $tiempo){

	$query = "INSERT INTO paquete(idPaquete, estado, rAsignado, dueno, tiempoAprox) VALUES(NULL, '$estado', $rAsignado, $dueno, $tiempo)";

        $result = mysqli_query($this->con, $query);

        return $result;

    }
    
    public function insertPedido($idPaquete, $idProducto, $cantidad){

	$query = "INSERT INTO r_paquete_pedido (idPaquete, idProducto, cantidad) VALUES($idPaquete, $idProducto, $cantidad)";

        $result = mysqli_query($this->con, $query);

        return $result;

    }
    
    public function getLastPaqueteFromUser($dueno){
    
	$query = " SELECT idPaquete FROM paquete WHERE dueno = ".$dueno." ORDER BY idPaquete DESC LIMIT 1";

        $result = mysqli_query($this->con, $query);

        return $result;
    }
    

    function getUserLocation($idUser){
    
        $query = "SELECT latUsuario, lonUsuario FROM usuarios WHERE idUsuario = ".$idUser;
	$result = mysqli_query($this->con, $query);

	return $result;
    

    }
    
     function getStoresLocation(){
    
        $query = "SELECT idTienda, latTienda, lonTienda FROM tienda";
	$result = mysqli_query($this->con, $query);

	return $result;
    

    }
    
    
     function getRepartidorFromStore($idTienda){

	$query = "SELECT idRepartidor FROM repartidores WHERE tAsignada = ".$idTienda." ORDER BY RAND() LIMIT 1";

	$result = mysqli_query($this->con, $query);

	return $result;

    } 
    
    function getRepartidor($idRepartidor){

	$query = "SELECT * FROM repartidores WHERE idRepartidor = ".$idRepartidor;
	$result = mysqli_query($this->con, $query);

	return $result;

    } 

    function getUser($idUser){

        $query = "SELECT * FROM usuarios WHERE idUsuario = ".$idUser;
        $result = mysqli_query($this->con, $query);

        return $result;

    }


    function existsRepartidor($idRepartidor, $password){



        $pass = md5($password);
        
	$query = 'SELECT COUNT(idRepartidor) AS existsRepartidor FROM repartidores WHERE idRepartidor = "'.$idRepartidor.'." AND pass="'.$pass.'"';

	$result = mysqli_query($this->con, $query);


        return $result;

    }


    function existsUser($idUser, $password){



        $pass = md5($password);

        $query = 'SELECT COUNT(idUsuario) AS existUser FROM usuarios WHERE idUsuario = "'.$idUser.'." AND pass="'.$pass.'"';

        $result = mysqli_query($this->con, $query);


        return $result;

    }
    
     function getProducts(){

	$query = "SELECT * FROM productos";
	$result = mysqli_query($this->con, $query);
	
	return $result;

    } 
    
    function getInforPaquete($idUser){

	$query = "SELECT paquete.idPaquete, paquete.tiempoAprox, repartidores.nombre, repartidores.latRepartidor, repartidores.lonRepartidor
                    FROM paquete INNER JOIN repartidores 
                    ON paquete.rAsignado = repartidores.idRepartidor
                 WHERE paquete.dueno = ".$idUser." AND paquete.estado <> 'pagado' AND paquete.estado <> 'cancelado'";
                 
	$result = mysqli_query($this->con, $query);
	
	return $result;

    } 


    
}

?>
