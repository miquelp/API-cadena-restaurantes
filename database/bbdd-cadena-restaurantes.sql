-- phpMyAdmin SQL Dump
-- version 4.7.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 27, 2018 at 01:35 PM
-- Server version: 5.7.22-0ubuntu0.16.04.1
-- PHP Version: 7.0.30-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sgbd`
--

-- --------------------------------------------------------

--
-- Table structure for table `paquete`
--

CREATE TABLE `paquete` (
  `idPaquete` int(11) NOT NULL,
  `estado` varchar(10) NOT NULL,
  `rAsignado` varchar(10) NOT NULL,
  `dueno` varchar(10) NOT NULL,
  `tiempoAprox` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `paquete`
--

INSERT INTO `paquete` (`idPaquete`, `estado`, `rAsignado`, `dueno`, `tiempoAprox`) VALUES
(1, 'pagado', '1', '1', 50),
(2, 'pagado', '1', '1', 50),
(3, 'en camino', '2', '43205975', 50),
(4, 'pagado', '2', '324124', 50),
(5, 'pagado', '2', '2', 50),
(6, 'pagado', '2', '43205975', 50),
(7, 'pagado', '2', '2', 50),
(8, 'en camino', '2', '324124', 50),
(9, 'en camino', '5', '2', 50),
(10, 'en camino', '5', '2', 50),
(46, 'preparando', '4', '4565433', 45),
(47, 'pagado', '4', '9871358', 41),
(48, 'preparando', '4', '2', 76),
(49, 'preparando', '1', '4', 5913);

-- --------------------------------------------------------

--
-- Table structure for table `productos`
--

CREATE TABLE `productos` (
  `idProducto` int(11) NOT NULL,
  `nombre` varchar(15) NOT NULL,
  `descripcion` tinytext NOT NULL,
  `precio` double NOT NULL,
  `tipo` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `productos`
--

INSERT INTO `productos` (`idProducto`, `nombre`, `descripcion`, `precio`, `tipo`) VALUES
(1, 'Margarita', 'Tomate, Mozzarela ', 5.5, 'pizza'),
(2, 'Nestea', 'Botella de Nestea de 1L', 2, 'bebida'),
(3, '4 quesos', 'Mozzarela, tomate, quesos de cabra, azul y parmesano', 6, 'pizza'),
(4, 'Prosciutto', 'Tomate, mozzarela, queso y jamon', 6.5, 'pizza'),
(5, 'Cocacola', 'Bebida de cola de 1L', 0.9, 'bebida');

-- --------------------------------------------------------

--
-- Table structure for table `repartidores`
--

CREATE TABLE `repartidores` (
  `idRepartidor` int(11) NOT NULL,
  `nombre` varchar(15) NOT NULL,
  `moto` varchar(10) NOT NULL,
  `tAsignada` int(11) NOT NULL,
  `latRepartidor` double DEFAULT NULL,
  `lonRepartidor` double DEFAULT NULL,
  `estado` varchar(20) NOT NULL DEFAULT 'esperando',
  `pass` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `repartidores`
--

INSERT INTO `repartidores` (`idRepartidor`, `nombre`, `moto`, `tAsignada`, `latRepartidor`, `lonRepartidor`, `estado`, `pass`) VALUES
(1, 'Mariano', 'JUJUSK9', 1, NULL, NULL, 'esperando', '49ca2c3fec9d0c72fa1ef48d0aca81bb'),
(2, 'Pedro', 'JUJUSK9', 2, 39.6521757, 2.774850000000015, 'esperando', 'aebaecd5a2305283a185254a35da9f12'),
(3, 'Miquel', 'JUJUSK9', 2, NULL, NULL, 'esperando', 'b4ch4t4'),
(4, 'Pablo', 'JUJUSK9', 3, 39.7209036, 2.910880700000007, 'esperando', '4d186321c1a7f0f354b297e8914ab240'),
(5, 'Marco', 'Harley', 1, 39, 2, 'esperando', '81dc9bdb52d04dc20036dbd8313ed055'),
(6, 'rep_prueba', '123', 1, 123, 321, 'esperando', '4d186321c1a7f0f354b297e8914ab240');

-- --------------------------------------------------------

--
-- Table structure for table `r_paquete_pedido`
--

CREATE TABLE `r_paquete_pedido` (
  `idPaquete` int(11) NOT NULL,
  `idProducto` int(11) NOT NULL,
  `cantidad` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `r_paquete_pedido`
--

INSERT INTO `r_paquete_pedido` (`idPaquete`, `idProducto`, `cantidad`) VALUES
(46, 1, 2),
(46, 2, 1),
(46, 5, 1),
(47, 2, 1),
(47, 4, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tienda`
--

CREATE TABLE `tienda` (
  `idTienda` int(11) NOT NULL,
  `latTienda` double NOT NULL,
  `lonTienda` double NOT NULL,
  `addr` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tienda`
--

INSERT INTO `tienda` (`idTienda`, `latTienda`, `lonTienda`, `addr`) VALUES
(1, 39.5732217, 2.6393602000000556, 'Carrer de Pou, 8, 07013 Palma'),
(2, 39.6098703, 2.6939366999999947, 'Carrer Alguer, 19, 07141 Palma'),
(3, 39.7209036, 2.910880700000007, 'Carrer de Pau, 39, 07300 Inca');

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE `usuarios` (
  `idUsuario` int(11) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `latUsuario` double DEFAULT NULL,
  `lonUsuario` double DEFAULT NULL,
  `pass` varchar(32) NOT NULL,
  `alias` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`idUsuario`, `nombre`, `latUsuario`, `lonUsuario`, `pass`, `alias`) VALUES
(2, 'prueba', 39.5697165, 3.209531800000036, '123456', 'pruebauser'),
(3, 'luis', 1, 2, '4d186321c1a7f0f354b297e8914ab240', 'luis'),
(5, 'Luis', 39.71777429999999, 2.907547499999964, '502ff82f7f1f8218dd41201fe4353687', 'luis'),
(1234, 'Miquel', 12, 21, '81dc9bdb52d04dc20036dbd8313ed055', 'mql2112'),
(123098, 'Test Test Test', 39.6338312, 2.7321047000000362, '4d186321c1a7f0f354b297e8914ab240', 'test3'),
(324124, 'Visi', 39.576355, 2.6541919999999664, 'holamundo1', 'v.ruiz'),
(678910, 'usercampanet', 39.7755984, 2.9612187, '4d186321c1a7f0f354b297e8914ab240', 'usercamp'),
(777777, 'Usuari Campanet', 39.774825299999996, 2.9654312, '4d186321c1a7f0f354b297e8914ab240', 'usuaricampanet'),
(912345, 'Luis Baquera', 39.649395899999995, 2.771697, '81dc9bdb52d04dc20036dbd8313ed055', 'luisbaquera1'),
(987654, 'Test User', 39.649395899999995, 2.771697, '81dc9bdb52d04dc20036dbd8313ed055', 'testuser123'),
(1234321, 'Test Test Prueba', 39.649395899999995, 2.771697, '4d186321c1a7f0f354b297e8914ab240', 'ttprueba'),
(3210952, 'Tomeu Estrany', 111, 222, '4d186321c1a7f0f354b297e8914ab240', 'tomeuestrany'),
(4565433, 'Us.Campanet', 39.774825299999996, 2.9654312, '4d186321c1a7f0f354b297e8914ab240', 'us.campanet'),
(9871234, 'Tomeu Estrany', 39.576355, 2.6541919999999997, '4d186321c1a7f0f354b297e8914ab240', 'tomeu123'),
(9871358, 'Us.Inca', 39.7499258, 2.8729104999999997, '4d186321c1a7f0f354b297e8914ab240', 'us.inca'),
(12345678, 'Miquel Perez Juan', 39.649395899999995, 2.771697, '4d186321c1a7f0f354b297e8914ab240', 'miquel123'),
(19283747, 'Paquito Paco', 39.649395899999995, 2.771697, '4d186321c1a7f0f354b297e8914ab240', 'paquito1'),
(43201022, 'Visi Ruiz', 39.641222, 2.6455590000000484, '87fb5d039a57c558122d7f70b7748c9f', 'visi.ruiz'),
(43205975, 'Miquel', 39.7670693, 2.7157866000000013, '01bb60aae77b30dc9c96db8df909967d', 'mql21'),
(444555666, 'Prova 123', 123, 321, '9450476b384b32d8ad8b758e76c98a69', 'prova321');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `paquete`
--
ALTER TABLE `paquete`
  ADD PRIMARY KEY (`idPaquete`);

--
-- Indexes for table `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`idProducto`);

--
-- Indexes for table `repartidores`
--
ALTER TABLE `repartidores`
  ADD PRIMARY KEY (`idRepartidor`);

--
-- Indexes for table `r_paquete_pedido`
--
ALTER TABLE `r_paquete_pedido`
  ADD PRIMARY KEY (`idPaquete`,`idProducto`);

--
-- Indexes for table `tienda`
--
ALTER TABLE `tienda`
  ADD PRIMARY KEY (`idTienda`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idUsuario`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `paquete`
--
ALTER TABLE `paquete`
  MODIFY `idPaquete` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
